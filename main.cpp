#include <iostream>
#include <vector>

// Variant 1 - return error codes.

int last_error_code = 0;

enum INT_DIV_ERRORS {
    SUCCESS = 0,
    DIV_BY_ZERO = -1
};

int int_div(int a, int b) {
    if(b!=0) {
        last_error_code = SUCCESS;
        return a/b;
    }
    else {
        last_error_code = DIV_BY_ZERO;
        return 0;
    }
}

int index_of_c(std::string s) {
    for (int i = 0; i < s.length(); ++i) {
        if(s[i] == 'c')
            return i;
    }
    return -1;
}

// variant 2 - try - catch - throw

class Bomb {
public:
    Bomb() {}
    Bomb(int v):val(v) {}

    Bomb(const Bomb &other) {
        if(other.val == 5) {
            throw "Boom!";
        }
        val = other.val;
    }

    int val;
};

class Stack {
private:
    std::vector<Bomb> data;
    int top = -1;
public:
    Bomb get_top() {
        if(top>=0) return data[top];
        else throw "Stack underflow!";
    }

    void pop() {
        if(top>=0) top--;
        else throw "Stack underflow!";
    }

    void push(Bomb value) {
        if(data.size() <= top+1) {
            data.resize(data.size() + 5);
        }
        data[++top] = value;
    }

    void print() {
        for(int i=0;i<=top;i++) {
            std::cout << data[i].val << " ";
        }
        std::cout << std::endl;
    }
};

class A {
public:
    int x;
    int y;
};

class CalculateException {
};

class DivByZeroException : public CalculateException {
public:
    DivByZeroException(int nom) : nom(nom) {}

    int nom;
};

class BothZeroException : public CalculateException {
};

int int_div_v2(int x, int y) {
    if(x > 10000) {
        throw CalculateException();
    }
    else if(y != 0) {
        return x/y;
    }
    else if(x == y && y == 0) {
        throw BothZeroException();
    }
    else {
        throw DivByZeroException(x);
    }
}

void testException() {
    try {
        int idx = int_div_v2(4, 0);
        std::cout << "Result: " << idx <<  std::endl;
    }
    catch(...) {
        std::cout << "Unknown exception!" << std::endl;
        throw;
    }
    std::cout << "Hello!" << std::endl;
}

int main() {
    try {
        testException();
    }
    catch(DivByZeroException ex)
    {
        std::cout << "Division by zero! Can't divide " << ex.nom << " by 0!" << std::endl;
//        throw;
    }
    catch(BothZeroException ex)
    {
        std::cout << "Both are zero!" << std::endl;
    }
    catch(CalculateException ex) {
        std::cout << "Some generic calculation exception catched!" << std::endl;
    }

    Stack s;
    try {
        int val;
        s.push(3);
        std::cout << "Pushed 3" << std::endl;
        s.push(4);
        std::cout << "Pushed 4" << std::endl;
        s.push(5);
        std::cout << "Pushed 5" << std::endl;
        val = s.get_top().val;
        std::cout << "Got top value " << val << std::endl;
        s.pop();
        std::cout << "Popped value" << val << std::endl;

        s.push(6);
        std::cout << "Pushed 6" << std::endl;
    }
    catch(...) {
        std::cout << "Exception happened!" << std::endl;
    }

    s.print();

    return 0;
}
